# Changelog
### 1.0.0
* Updated mod for Avorion 2.*

### 0.1.2
* Updated mod for Avorion 1.0
* Added new config: fixedRespawnTime use a fix time until Scout respawns instead of event depending timer (requires Avorion 1.0.1 or higher)

### 0.1.1
* Updated mod for Avorion 0.29

### 0.1
* Removed from HGS mod and released as standalone mod 