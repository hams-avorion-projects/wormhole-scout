package.path = package.path .. ";data/scripts/lib/?.lua"
package.path = package.path .. ";data/scripts/lib/ConfigLib/?.lua"

local SectorGenerator = include ("SectorGenerator")

local ConfigLib = include("ConfigLib")
local WormholeScoutConfigLib = ConfigLib("1770546258")


local WormholeScoutCreateCarrier = Xsotan.createCarrier
function Xsotan.createCarrier(position, volumeFactor, fighters)
	if WormholeScoutConfigLib.get("xsotanFighterAmount") == 0 then
		return Xsotan.createShip(position, volumeFactor)
	end
	
	fighters = fighters or 30
	return WormholeScoutCreateCarrier(position, volumeFactor, math.ceil(fighters * (WormholeScoutConfigLib.get("xsotanFighterAmount") / 100)))
end



function Xsotan.createPlasmaTurret()
	local turret = SectorTurretGenerator(Seed(151)):generate(0, 0, 0, Rarity(RarityType.Uncommon), WeaponType.PlasmaGun)

    local weapons = {turret:getWeapons()}
    turret:clearWeapons()
	turret.coaxial = false
    for _, weapon in pairs(weapons) do
		weapon.damage = weapon.damage / 10
        weapon.reach = WormholeScoutConfigLib.get("allWeaponRange") * 100
        weapon.pmaximumTime = weapon.reach / weapon.pvelocity
        turret:addWeapon(weapon)
    end

    turret.turningSpeed = 2.0
    turret.crew = Crew()

    return turret
end

function Xsotan.createLaserTurret()
    local turret = SectorTurretGenerator(Seed(152)):generate(0, 0, 0, Rarity(RarityType.Exceptional), WeaponType.Laser)
    local weapons = {turret:getWeapons()}
    turret:clearWeapons()
	turret.coaxial = false
    for _, weapon in pairs(weapons) do
		weapon.damage = weapon.damage / 10
        weapon.reach = WormholeScoutConfigLib.get("allWeaponRange") * 100
        weapon.blength = WormholeScoutConfigLib.get("allWeaponRange") * 100
        turret:addWeapon(weapon)
    end

    turret.turningSpeed = 2.0
    turret.crew = Crew()

    return turret
end

function Xsotan.createRailgunTurret()
    local turret = SectorTurretGenerator(Seed(153)):generate(0, 0, 0, Rarity(RarityType.Uncommon), WeaponType.RailGun)
    local weapons = {turret:getWeapons()}
    turret:clearWeapons()
	turret.coaxial = false
    for _, weapon in pairs(weapons) do
		weapon.damage = weapon.damage / 10
        weapon.reach = WormholeScoutConfigLib.get("allWeaponRange") * 100 * 1.5
        turret:addWeapon(weapon)
    end

    turret.turningSpeed = 2.0
    turret.crew = Crew()

    return turret
end

function Xsotan.createPulseTurret()
	local turret = SectorTurretGenerator(Seed(150)):generate(0, 0, 0, Rarity(RarityType.Common), WeaponType.PulseCannon)
    local weapons = {turret:getWeapons()}
    turret:clearWeapons()
	turret.coaxial = false
    for _, weapon in pairs(weapons) do
		weapon.damage = weapon.damage / 10
        weapon.reach = WormholeScoutConfigLib.get("allWeaponRange") * 100
        weapon.pmaximumTime = weapon.reach / weapon.pvelocity
        turret:addWeapon(weapon)
    end

    turret.turningSpeed = 2.0
    turret.crew = Crew()

    return turret
end


local WormholeScoutCreateGuardian = Xsotan.createGuardian
function Xsotan.createGuardian(position, volumeFactor, isBoss)
	local x, y = Sector():getCoordinates()
	
    volumeFactor = (volumeFactor or 10)
	if isBoss then -- Guardian
		volumeFactor = volumeFactor * WormholeScoutConfigLib.get("guardianVolume")
	else -- Scout
		volumeFactor = volumeFactor * WormholeScoutConfigLib.get("scoutVolume")
	end
	
	local boss = WormholeScoutCreateGuardian(position, volumeFactor)
	
	if isBoss and WormholeScoutConfigLib.get("guardianPulse") then
		local numTurrets = math.max(1, Balancing_GetEnemySectorTurrets(x, y) / 2)
		ShipUtility.addTurretsToCraft(boss, Xsotan.createPulseTurret(), numTurrets, numTurrets)
	end
	
	if isBoss and WormholeScoutConfigLib.get("guardianTorps") then -- Highly experimental
		ShipUtility.addTorpedoBoatEquipment(boss)
	end
	
	boss.damageMultiplier = boss.damageMultiplier * 15 -- Weapon damage is reduced, lets rebalance it to vanilla dps (dont know why we need *15 here...)
    
	if isBoss then -- Guardian
		boss:setValue("is_guardian", 1) -- Is boss
		boss.damageMultiplier = boss.damageMultiplier * WormholeScoutConfigLib.get("guardianDamage")
		
		-- Additional loot for the Guardian (Scout uses vanilla loot)
		local upgrades =
		{
			{rarity = Rarity(RarityType.Legendary), amount = 1},
			{rarity = Rarity(RarityType.Exotic), amount = 1},
			{rarity = Rarity(RarityType.Exceptional), amount = 2},
			{rarity = Rarity(RarityType.Rare), amount = 3},
			{rarity = Rarity(RarityType.Uncommon), amount = 4},
			{rarity = Rarity(RarityType.Common), amount = 6},
		}

		local turrets =
		{
			{rarity = Rarity(RarityType.Legendary), amount = 1},
			{rarity = Rarity(RarityType.Exotic), amount = 1},
			{rarity = Rarity(RarityType.Exceptional), amount = 2},
			{rarity = Rarity(RarityType.Rare), amount = 3},
			{rarity = Rarity(RarityType.Uncommon), amount = 4},
			{rarity = Rarity(RarityType.Common), amount = 6},
		}


		local generator = UpgradeGenerator()
    for _, p in pairs(upgrades) do
        for i = 1, p.amount do
            Loot(boss.index):insert(generator:generateSectorSystem(x, y, p.rarity))
        end
    end

    for _, p in pairs(turrets) do
        for i = 1, p.amount do
            Loot(boss.index):insert(InventoryTurret(SectorTurretGenerator():generate(x, y, 0, p.rarity)))
        end
    end
	else -- Scout
		boss:setValue("is_scout", 1) -- Is scout
		boss.damageMultiplier = boss.damageMultiplier * WormholeScoutConfigLib.get("scoutDamage")
		boss.title = "Xsotan Wormhole Scout"%_t
	end
	
	boss.crew = boss.minCrew
    boss.shieldDurability = boss.shieldMaxDurability

    return boss
end


function Xsotan.createBeacon()
    local beacon = SectorGenerator(0, 0):createBeacon(Matrix(), nil, "Scanners online."%_t)
    beacon:addScriptOnce("story/wormholeguardianbeacon")
    beacon:addScriptOnce("deleteonplayersleft")
end




