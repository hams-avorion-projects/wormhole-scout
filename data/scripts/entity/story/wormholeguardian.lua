package.path = package.path .. ";data/scripts/lib/ConfigLib/?.lua"

local ConfigLib = include("ConfigLib")
local WormholeScoutConfigLib = ConfigLib("1770546258")

local initialized = false

WormholeGuardian.shieldLoss = 0.25 -- This changed to a shield heal
WormholeGuardian.spawnAllyFrequency = 0

WormholeGuardian.isBoss = false


local WormholeScoutInitialize = WormholeGuardian.initialize
function WormholeGuardian.initialize()
	
	
	Entity():addAbsoluteBias(StatsBonuses.ShieldImpenetrable, 1)
	Entity():addBaseMultiplier(StatsBonuses.ShieldRecharge, 9999999)
	
	WormholeScoutInitialize()
end


local WormholeScoutUpdateServer = WormholeGuardian.updateServer
function WormholeGuardian.updateServer(timePassed)
	local entity = Entity()
	
	-- We need delayed initialize() - run on first update time frame
	-- Delayed initialize() start
	if not initialized then
		initialized = true
		if WormholeGuardian.getIsBoss() and WormholeScoutConfigLib.get("guardianShield") > 1 then
			entity:addKeyedMultiplier(StatsBonuses.ShieldDurability, 17705462581, WormholeScoutConfigLib.get("guardianShield"))
		elseif not WormholeGuardian.getIsBoss() and  WormholeScoutConfigLib.get("scoutShield") > 1 then
			entity:addKeyedMultiplier(StatsBonuses.ShieldDurability, 17705462581, WormholeScoutConfigLib.get("scoutShield"))
		end
		Entity().shieldDurability = entity.shieldMaxDurability
		WormholeGuardian.shieldDurability = entity.shieldMaxDurability
		
		-- Set add spawn frequency
		if WormholeGuardian.getIsBoss() then
			WormholeGuardian.spawnAllyFrequency = 5
		else
			WormholeGuardian.spawnAllyFrequency = 7.5
		end
	end
	-- Delayed initialize() stop
	
	if entity.shieldDurability <= (4) then
		entity:damageShield(4, vec3(), Uuid()) -- prevent auto shield recharge
	end
	
	
	
	WormholeScoutUpdateServer(timePassed)
end


function WormholeGuardian.getIsBoss()
	if not WormholeGuardian.isBoss then
		WormholeGuardian.isBoss = tonumber(Entity():getValue("is_guardian"))
	end
	
	return WormholeGuardian.isBoss == 1
end


local WormholeScoutOnDestroyed = WormholeGuardian.onDestroyed
function WormholeGuardian.onDestroyed()
	if WormholeScoutConfigLib.get("fixedRespawnTime") > 0 then
		Server():setValue("guardian_fixed_respawn_time", WormholeScoutConfigLib.get("fixedRespawnTime") * 60)
	end
	
	if not WormholeGuardian.getIsBoss() then
		Xsotan.createBeacon()
		Server():setValue("guardian_respawn_time", 60 * 60) -- Avoid insta respawn, but do not trigger event
	else
		WormholeScoutOnDestroyed()
	end
end


local WormholeScoutSetChanneling = WormholeGuardian.setChanneling
function WormholeGuardian.setChanneling()
	if WormholeGuardian.getIsBoss() then
		WormholeScoutSetChanneling()
	else
		WormholeGuardian.channelStateTime = 0
		WormholeGuardian.state = State.Channeling
		WormholeGuardian.createChannelBeam()

		Sector():broadcastChatMessage("", 2, "The scout is starting to channel the black hole's energy!"%_t)
	end
end


function WormholeGuardian.channel(timePassed)
    ShipAI():setPassive()
	local entity = Entity()
	if Entity().shieldDurability > (4) then
		entity:healShield(timePassed / (WormholeGuardian.spawningStateDuration + WormholeGuardian.channelDuration) * WormholeGuardian.shieldLoss * entity.shieldMaxDurability)
	end
end



function WormholeGuardian.createXsotan()
    if #WormholeGuardian.xsotanWormholes == 0 then return end

    -- pick a random wormhole
    local wormhole = WormholeGuardian.xsotanWormholes[random():getInt(1, #WormholeGuardian.xsotanWormholes)]

    if not valid(wormhole) then return end
	
	local isBoss = WormholeGuardian.getIsBoss()
	local shipsize = 1
	
	if isBoss then
		shipsize = shipsize * 1.5
	end

    local spawn = 1
    local spawned = {}
    for i = 1, spawn do
        local ally
        if Entity().durability < Entity().maxDurability * 0.75 and random():getInt(1, 2) == 1 then
            ally = Xsotan.createCarrier(wormhole.position, 2.0 * shipsize, 5)
        else
            ally = Xsotan.createShip(wormhole.position, 1.0 * shipsize)
        end
		
		if isBoss then
			ally.damageMultiplier = ally.damageMultiplier * math.max((WormholeScoutConfigLib.get("guardianDamage") / 2), 2)
		end

        table.insert(spawned, ally)
    end

    Placer.resolveIntersections(spawned)
    WormholeGuardian.aggroAllies()
end