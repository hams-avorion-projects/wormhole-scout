package.path = package.path .. ";data/scripts/lib/?.lua"

Xsotan = include("story/xsotan")
include ("stringutility")
include ("callable")

local interactable = true

function initialize()
    Entity().title = "Scanner Beacon"%_t
end

function interactionPossible(playerIndex, option)
    return interactable
end

function initUI()
    ScriptUI():registerInteraction("Activate"%_t, "onActivate")
end

function spawnGuardian()
    if onClient() then
        invokeServerFunction("spawnGuardian")
        return
    end
	
	local sector = Sector()
	 -- clear everything that's not player owned
    local entities = {sector:getEntities()}
    for _, entity in pairs(entities) do
        if entity.factionIndex and not Player(entity.factionIndex) and (Faction(entity.factionIndex) and not Faction(entity.factionIndex).isAlliance) then
            sector:deleteEntity(entity)
        end
    end

    Xsotan.createGuardian(nil, nil, true)

    terminate()
end
callable(nil, "spawnGuardian")

function onActivate()
    local dialog = {}
    dialog.text = "Scanning..."%_t

    local positive = {}
    positive.text = "Nothing happens... where is the Guardian?"%_t
    positive.followUp = {text = "The subspace signals are getting too strong for your scanners. Brace yourself!"%_t, onEnd = "spawnGuardian"}

    dialog.followUp = positive


    ScriptUI():showDialog(dialog)
end
