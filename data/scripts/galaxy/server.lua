package.path = package.path .. ";data/scripts/lib/ConfigLib/?.lua"

local ConfigLib = include("ConfigLib")
local WormholeScoutConfigLib = ConfigLib("1770546258")

-- guardian_fixed_respawn_time:   Fixed guardian respawn time, implemented by this mod
-- guardian_respawn_time:   Time between Xsotan Swarm Event was lost and respawn of Guardian (line 64)
-- xsotan_swarm_time:   Time between Guardian was killed and Xsotan Event spawns (set in wormholeguardian.lua:77 in onDestroyed())
--                      Also time between Xsotan Event was won and restarts (line 53)
-- xsotan_swarm_duration:   Base time player have to complete the event, is increased by 10 min if Guardian Precursor spawns (line 40)
local WormholeScoutUpdate = update
function update(timeStep)
	if WormholeScoutConfigLib.get("fixedRespawnTime") > 0 then
		local server = Server()

		local fixedGuardianRespawnTime = server:getValue("guardian_fixed_respawn_time")
		if fixedGuardianRespawnTime then

			fixedGuardianRespawnTime = fixedGuardianRespawnTime - timeStep
			if fixedGuardianRespawnTime < 0 then
				fixedGuardianRespawnTime = nil
				server:broadcastChatMessage("Server", ChatMessageType.Information, "Strong subspace disturbances have been detected. They seem to be originating from the center of the galaxy."%_T)
				server:setValue("guardian_respawn_time", nil) -- Guardian just respawned, unset vanilla timer
			end

			server:setValue("guardian_fixed_respawn_time", fixedGuardianRespawnTime)
			
		end
    end
	
	WormholeScoutUpdate(timeStep)
end

