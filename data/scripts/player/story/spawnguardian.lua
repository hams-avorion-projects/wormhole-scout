package.path = package.path .. ";data/scripts/lib/ConfigLib/?.lua"


if onServer() then

local ConfigLib = include("ConfigLib")
local WormholeScoutConfigLib = ConfigLib("1770546258")

local WormholeScoutCanSpawn = SpawnGuardian.canSpawn
function SpawnGuardian.canSpawn()
    if WormholeScoutConfigLib.get("fixedRespawnTime") == 0 then
		return WormholeScoutCanSpawn()
	end
	
	if Server():getValue("guardian_fixed_respawn_time") then
		return false
	end

    return true
end


end
